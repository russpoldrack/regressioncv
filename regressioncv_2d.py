import numpy as N
from sklearn.linear_model import LinearRegression
from sklearn import cross_validation
#from statsmodels.formula.api import ols
from statsmodels.regression.linear_model import OLS
import sys
import pickle

if len(sys.argv)>1:
    nsubs=int(sys.argv[1])
else:
    nsubs=20
    
nruns=1000


def get_sample_balcv(x,y,nfolds,pthresh=0.9):
    # create kfolds and test for equality of means in X
    # across folds - to balance distribution of X values
    nsubs=len(x)
    good_split=0
    while good_split==0:
        cv=cross_validation.KFold(n=nsubs,n_folds=nfolds,shuffle=True)
        ctr=0
        idx=N.zeros((nsubs,nfolds))
        for train,test in cv:
            idx[test,ctr]=1
            ctr+=1

        lm=OLS(x-N.mean(x),idx).fit()
#        print lm.summary()
#        print 'PValue:%f'%lm.f_pvalue
#        for train,test in cv:
#            print N.mean(x_all[test])
        if lm.f_pvalue>pthresh:
            #print '%f - good split!'%lm.f_pvalue
            good_split=1
            #print lm.params
            #else:
            #    print '%f - skipping'%lm.pvalues['X']
    x=x.reshape((nsubs,1))
    y=y.reshape((nsubs,1))
    pred=N.zeros((nsubs,1))
    for train,test in cv:
        lr=LinearRegression()
        lr.fit(x[train,:],y[train,:])
        pred[test]=lr.predict(x[test])
        #
    #    data={'X':pred,'Y':y[:,0]}
    #    lm=ols('Y ~ X',data).fit()
    #    return lm.params['X']
    return N.corrcoef(pred[:,0],y[:,0])[0,1]


corrs={'splithalf':N.zeros(nruns),'splitcv':N.zeros(nruns),'loo':N.zeros(nruns),'balcv_lo':N.zeros(nruns),'balcv_hi':N.zeros(nruns)}




print 'running for %d subs'%nsubs
for run in range(nruns):
    x=N.random.rand(nsubs/2).reshape((nsubs/2,1))

    y=N.random.rand(len(x)).reshape(x.shape)
    y2=N.random.rand(len(x)).reshape(x.shape)
    pred_y=N.zeros(x.shape)
    pred_y2=N.zeros(x.shape)

    lr=LinearRegression()
    lr.fit(x,y)
    pred_y2=lr.predict(x)
    lr.fit(x,y2)
    pred_y=lr.predict(x)

    pred_splithalf=N.vstack((pred_y,pred_y2))
    y_all=N.vstack((y,y2))
#    corrs_y[run,nsubs_idx]=N.corrcoef(pred_y.T,y.T)[0,1]
#    corrs_y2[run]=N.corrcoef(pred_y2.T,y2.T)[0,1]
    corrs['splithalf'][run]=N.corrcoef(pred_splithalf[:,0],y_all[:,0])[0,1]

    x_all=N.vstack((x,x))
    split=cross_validation.KFold(n=len(x_all),n_folds=2,shuffle=False)
    pred_splitcv=N.zeros(x_all.shape)
    for train,test in split:
        lr=LinearRegression()
        lr.fit(x_all[train,:],y_all[train,:])
        pred_splitcv[test]=lr.predict(x_all[test])
    corrs['splitcv'][run]=N.corrcoef(pred_splitcv[:,0],y_all[:,0])[0,1]

    loo=cross_validation.LeaveOneOut(nsubs)
    pred_loo=N.zeros(x_all.shape)
    for train,test in loo:
        lr=LinearRegression()
        lr.fit(x_all[train,:],y_all[train,:])
        pred_loo[test]=lr.predict(x_all[test])
    corrs['loo'][run]=N.corrcoef(pred_loo[:,0],y_all[:,0])[0,1]

    corrs['balcv_lo'][run]=get_sample_balcv(x_all,y_all,8,pthresh=0.001)
    corrs['balcv_hi'][run]=get_sample_balcv(x_all,y_all,8,pthresh=0.99)


f=open('loosim_random_data_%d_subs.pkl'%nsubs,'wb')
pickle.dump(corrs,f)
f.close()

